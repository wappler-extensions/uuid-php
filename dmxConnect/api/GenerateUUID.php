<?php
require('../../dmxConnectLib/dmxConnect.php');


$app = new \lib\App();

$app->define(<<<'JSON'
{
  "name": "UUID",
  "module": "GenerateUID",
  "action": "GetUUID",
  "options": {},
  "output": true
}
JSON
);
?>