<?php 
namespace modules;
use \lib\core\Module;
require_once __DIR__ . '/../../../vendor/autoload.php';
use Ramsey\Uuid\Uuid;

class GenerateUID extends Module
{
    public function GetUUID()
    {
        $uuid = Uuid::uuid4();
        return $uuid->toString();
    }
}
?>